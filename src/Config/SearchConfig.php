<?php

namespace Firesphere\SearchBackend\Config;

use SilverStripe\Core\Config\Configurable;
use SilverStripe\Core\Environment;

class SearchConfig
{
    use Configurable;

    public const SOLR_CONFIG = 'SOLR';

    public const ELASTIC_CONFIG = 'ELASTIC';

    /**
     * Map the ENV variables to the wanted names of the config
     * @var array
     */
    private const ENVIRONMENT_VARS = [
        '_ENDPOINT' => 'host',
        '_USERNAME' => 'username',
        '_PASSWORD' => 'password',
        '_PORT'     => 'port',
        '_API_KEY'  => 'apiKey',
        '_PROTOCOL' => 'protocol'
    ];


    public static function getConfig($type)
    {
        $type = strtoupper($type);
        $config = self::config()->get('config');
        if (!isset($config['endpoint']) || $config['endpoint'] === 'ENVIRONMENT') {
            $endpoint0 = [];
            foreach (self::ENVIRONMENT_VARS as $envVar => $connectionVar) {
                $envVar = $type . $envVar;
                if (Environment::hasEnv($envVar)) {
                    $endpoint0[$connectionVar] = Environment::getEnv($envVar);
                }
            }
        } else {
            $endpoint0 = array_shift($config['endpoint']);
        }

        return $endpoint0;
    }
}
