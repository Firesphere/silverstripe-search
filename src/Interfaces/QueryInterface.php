<?php

namespace Firesphere\SearchBackend\Interfaces;

interface QueryInterface
{

    /**
     * @return array
     */
    public function getTerms(): array;

    /**
     * @param $terms
     * @return self
     */
    public function setTerms($terms): self;

    /**
     * Add a term to search on.
     * Note, each boosted query needs a separate addition!
     * e.g. $this->addTerm('test', ['MyField', 'MyOtherField'], 3)
     * followed by
     * $this->addTerm('otherTest', ['Title'], 5);
     *
     * If you want a generic boost on all terms, use addTerm only once, but boost on each field
     *
     * The fields parameter is used to boost on
     * @param string $term Term to search for
     * @param array $fields fields to boost on
     * @param int $boost Boost value
     * @param float|bool|null $fuzzy True or a value to the maximum amount of iterations
     * @return $this
     */
    public function addTerm(string $term, array $fields = [], int $boost = 1, float|bool $fuzzy = null): self;

    /**
     * @return array
     */
    public function getFilters(): array;

    /**
     * @param array $filters
     * @return self
     */
    public function setFilters(array $filters): self;

    /**
     * @param string $key
     * @param string $value
     * @return self
     */
    public function addFilter(string $key, string $value): self;

    /**
     * @return array
     */
    public function getOrFilters(): array;

    /**
     * @param array $filters
     * @return self
     */
    public function setOrFilters(array $filters): self;

    /**
     * @param string $key
     * @param string $value
     * @return self
     */
    public function addOrFilter(string $key, string $value): self;

    /**
     * Get the offset to start
     *
     * @return int
     */
    public function getStart(): int;

    /**
     * Set the offset to start
     *
     * @param int $start
     * @return self
     */
    public function setStart($start): self;

    /**
     * Get the rows to return
     *
     * @return int
     */
    public function getRows(): int;

    /**
     * Set the rows to return
     *
     * @param int $rows
     * @return self
     */
    public function setRows($rows): self;

    /**
     * Get the sort fields
     *
     * @return array
     */
    public function getSort(): array;

    /**
     * Set the sort fields
     *
     * @param array $sort
     * @return self
     */
    public function setSort($sort): self;

    /**
     * Add a sort field and direction
     *
     * @param string $field
     * @param string $direction
     * @return self
     */
    public function addSort($field, $direction): self;

    /**
     * @return array
     */
    public function getBoostedFields(): array;

    /**
     * @param array $boostedFields
     * @return self
     */
    public function setBoostedFields(array $boostedFields): self;

    /**
     * @param $key
     * @param $value
     * @return self
     */
    public function addBoostedField($key, $value): self;

    /**
     * @return bool
     */
    public function isHighlight(): bool;

    /**
     * @param bool $highlight
     * @return self
     */
    public function setHighlight(bool $highlight): self;
}
